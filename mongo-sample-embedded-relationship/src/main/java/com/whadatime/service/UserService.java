package com.whadatime.service;

import java.util.List;

import com.whadatime.domain.User;

public interface UserService extends IService<User> {
	List<User> findUserByRole(String name);
}