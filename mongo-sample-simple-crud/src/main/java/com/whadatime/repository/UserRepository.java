package com.whadatime.repository;

import java.util.UUID;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.whadatime.domain.User;

public interface UserRepository extends MongoRepository<User, UUID> {

}
