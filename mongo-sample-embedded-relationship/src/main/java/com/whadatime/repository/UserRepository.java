package com.whadatime.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.whadatime.domain.User;

public interface UserRepository extends MongoRepository<User, UUID> {
	@Query("{'Role.name':?0}")
	List<User> findUserByRole(String name);
}
