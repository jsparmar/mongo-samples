package com.whadatime.service.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.whadatime.domain.User;
import com.whadatime.exception.DataException;
import com.whadatime.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws DataException {
		log.info("UserDetailsServiceImpl : loadUserByUsername");
		User user = userRepository.findByEmail(email);
		if (user == null) {
			throw new DataException(email + " not found");
		}
		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
				getGrantedAuthority(user));
	}

	private Collection<GrantedAuthority> getGrantedAuthority(User user) {
		Collection<GrantedAuthority> authorities = new ArrayList<>();
		user.getRoles().forEach((role) -> {
			authorities.add(new SimpleGrantedAuthority(role.getName().toUpperCase()));
		});
		return authorities;
	}

}