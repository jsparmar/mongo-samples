package com.whadatime.config;

import java.io.Serializable;
import java.util.Base64;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class JwtTokenProvider implements Serializable {

	private static final long serialVersionUID = 2569800841756370596L;

	@Value("${jwt.secret-key}")
	private String secretKey;

	@PostConstruct
	protected void init() {
		secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
	}

	private static final long JWT_TOKEN_VALIDITY = 10 * 24 * 60 * 60;

	public String createToken(String username, String displayName, String role) {
		log.info("JwtTokenProvider : createToken");
		Claims claims = Jwts.claims().setSubject(username);
		claims.put("displayName", displayName);
		claims.put("role", role);

		long currentTimeMillis = System.currentTimeMillis();
		return Jwts.builder().setClaims(claims).setIssuedAt(new Date(currentTimeMillis))
				.setExpiration(new Date(currentTimeMillis + JWT_TOKEN_VALIDITY * 1000))
				.signWith(SignatureAlgorithm.HS512, secretKey).compact();
	}

	@Autowired
	private UserDetailsService userDetailsService;

	public Authentication getAuthentication(String username) {
		log.info("JwtTokenProvider : getAuthentication");
		UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		return new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword(),
				userDetails.getAuthorities());
	}

	public Claims getClaimsFromToken(String token) {
		log.info("JwtTokenProvider : getClaimsFromToken");
		return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
	}

}