package com.whadatime.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.whadatime.domain.User;

public interface UserService extends IService<User> {
	User updatePartial(UUID id, Map<Object, Object> fields);
	
	String authenticate(User user);

	User findByEmail(String email);

	List<User> findUserByRole(String name);
}