package com.whadatime.controller;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.whadatime.domain.User;
import com.whadatime.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;

	@DeleteMapping("{id}")
	public ResponseEntity<String> deleteById(@PathVariable UUID id) {
		return ResponseEntity.ok(userService.deleteById(id));
	}

	@PutMapping
	public ResponseEntity<User> update(@RequestBody User user) {
		return ResponseEntity.ok(userService.update(user));
	}
	
	@PatchMapping("{id}")
	public ResponseEntity<User> updatePartial(@PathVariable UUID id, @RequestBody Map<Object, Object> fields) {
		return ResponseEntity.ok(userService.updatePartial(id, fields));
	}

	@PostMapping("/authenticate")
	public ResponseEntity<String> authenticate(@RequestBody User user) {
		return ResponseEntity.ok(userService.authenticate(user));
	}

	@PostMapping("/register")
	public ResponseEntity<User> save(@RequestBody User user) {
		return ResponseEntity.ok(userService.save(user));
	}

	@GetMapping("/forgotpassword/{email}")
	public ResponseEntity<User> forgotPassword(@PathVariable String email) {
		return ResponseEntity.ok(null);
	}

	@GetMapping
	public ResponseEntity<List<User>> findAll() {
		return ResponseEntity.status(HttpStatus.OK).body(userService.findAll());
	}

	@GetMapping("{id}")
	public ResponseEntity<User> findById(@PathVariable UUID id) {
		return ResponseEntity.status(HttpStatus.OK).body(userService.findById(id).get());
	}

	@GetMapping("/role/{name}")
	public ResponseEntity<List<User>> findUserByRole(@PathVariable String name) {
		return ResponseEntity.status(HttpStatus.OK).body(userService.findUserByRole(name));
	}
}
