#Swagger Documentation
	http://localhost:8080/swagger-ui.html

#GET - 
	http://localhost:8080/user

#POST - 
	http://localhost:8080/user
	
	{
		"userName": "jeetsparmar",
		"fullName":"Jeet Singh Parmar",
		"email": "jeet@whadatime.com",
		"password": "123456",
		"role": [{"name":"admin"}, {"name":"user"}]
	}

	{
		"userName": "jeetsparmar",
		"fullName":"Jeet Singh Parmar",
		"email": "jeet@whadatime.com",
		"password": "123456",
		"role": [{"name":"admin"}]
	}
	
	{
		"userName": "jeetsparmar",
		"fullName":"Jeet Singh Parmar",
		"email": "jeet@whadatime.com",
		"password": "123456",
		"role": [{"name":"user"}]
	}
	
#PUT - 
	http://localhost:8080/user

	{
		"id": "7cb1042f-3903-41c5-8f52-61657315f67b",
		"userName": "jeetparmar",
		"fullName":"Jeet Singh Parmar",
		"email": "jeet@whadatime.com",
		"password": "123456",
		"role": [{"name": "user"}]
	}
	
#Find User by Role Name
	http://localhost:8080/user/role/admin
	http://localhost:8080/user/role/user
	
#DELTE
	http://localhost:8080/user/fce285d2-6d04-400d-a197-b3dba44a2bf1	