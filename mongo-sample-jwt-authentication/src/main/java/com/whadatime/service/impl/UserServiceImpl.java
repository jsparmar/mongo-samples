package com.whadatime.service.impl;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import com.whadatime.config.JwtTokenProvider;
import com.whadatime.domain.User;
import com.whadatime.exception.DataException;
import com.whadatime.repository.UserRepository;
import com.whadatime.service.UserService;
import com.whadatime.utils.MethodUtils;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenProvider tokenProvider;

	@Autowired
	private UserRepository userRepository;

	@Override
	public String authenticate(User user) {
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));
		if (authentication.isAuthenticated()) {
			String email = user.getEmail();
			User savedUser = findByEmail(email);
			String roles = savedUser.getRoles().stream().map(role -> role.getName()).collect(Collectors.joining(","));
			if (savedUser.getIsActive()) {
				return MethodUtils.convertStringToJSON("token",
						tokenProvider.createToken(email, savedUser.getFullName(), roles));
			} else {
				throw new DataException("User is not active.");
			}
		} else {
			return MethodUtils.convertStringToJSON("message", "Authentication Failed, Please try again.");
		}
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public Optional<User> findById(UUID id) {
		return Optional.ofNullable(userRepository.findById(id).orElseThrow(() -> new DataException("Invalid User ID")));
	}

	@Override
	public String deleteById(UUID id) {
		Optional<User> user = findById(id);
		userRepository.delete(user.get());
		return MethodUtils.convertStringToJSON("message", "User Deleted Successfully");
	}

	@Override
	public User save(User user) {
		user.setId(UUID.randomUUID());
		user.setIsActive(true);
		user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
		user.getRoles().forEach(role -> role.setName(role.getName().toUpperCase()));
		return userRepository.save(user);
	}

	@Override
	public User updatePartial(UUID id, Map<Object, Object> fields) {
		Optional<User> user = findById(id);
		if (user.isPresent()) {
			fields.forEach((key, value) -> {
				Field field = ReflectionUtils.findField(User.class, (String) key);
				field.setAccessible(true);
				ReflectionUtils.setField(field, user.get(), value);
			});
			return update(user.get());
		}
		throw new DataException("User ID is invalid.");
	}

	@Override
	public User update(User user) {
		return userRepository.save(user);
	}

	@Override
	public List<User> findUserByRole(String name) {
		return userRepository.findUserByRole(name.toUpperCase());
	}

}
