#Swagger Documentation
	http://localhost:8080/whadatime/rest/swagger-ui

#GET - 
	http://localhost:8080/whadatime/rest/user

#POST - 
	http://localhost:8080/whadatime/rest/user/register
	
Request

	{
		"userName": "jeetsparmar",
		"fullName":"Jeet Singh Parmar",
		"email": "jeet@whadatime.com",
		"password": "123456",
		"role": [{"name":"admin"}, {"name":"user"}]
	}

	{
		"userName": "jeetsparmar",
		"fullName":"Jeet Singh Parmar",
		"email": "jeet@whadatime.com",
		"password": "123456",
		"role": [{"name":"admin"}]
	}
	
	{
		"userName": "jeetsparmar",
		"fullName":"Jeet Singh Parmar",
		"email": "jeet@whadatime.com",
		"password": "123456",
		"role": [{"name":"user"}]
	}
	
#PUT - 
	http://localhost:8080/whadatime/rest/user

Request - Need to pass the entire object with updated value

	{
		"id": "7cb1042f-3903-41c5-8f52-61657315f67b",
		"userName": "jeetparmar",
		"fullName":"Jeet Singh Parmar",
		"email": "jeet@whadatime.com",
		"password": "123456",
		"role": [{"name": "user"}]
	}

#PATCH
	http://localhost:8080/whadatime/rest/user/0aa17899-3273-44e3-b3bb-9dcfd3975851
	
Request - Need to pass the partial object with updated value

	{ 
		"userName": "jeetsinghparmar"
	}
	
#Find User by Role Name
	http://localhost:8080/whadatime/rest/user/role/admin
	http://localhost:8080/whadatime/rest/user/role/user
	
#DELTE
	http://localhost:8080/whadatime/rest/user/fce285d2-6d04-400d-a197-b3dba44a2bf1	