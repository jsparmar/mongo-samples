#Swagger Documentation
	http://localhost:8080/swagger-ui.html

#GET - 
	http://localhost:8080/user

#POST - 
	http://localhost:8080/user

	{
		"userName": "jsparmar",
		"fullName":"Jeet Singh Parmar",
		"email": "jeet@whadatime.com",
		"password": "123456"
	}
	
#PUT - 
	http://localhost:8080/user

	{
		"id": "fce285d2-6d04-400d-a197-b3dba44a2bf1",
		"userName": "jeetsparmar",
		"fullName":"Jeet Singh Parmar",
		"email": "jeet@whadatime.com",
		"password": "123456"
	}
	
#DELTE
	http://localhost:8080/user/fce285d2-6d04-400d-a197-b3dba44a2bf1